from django.conf.urls import url, include
from venueapp import urls as apiurls

urlpatterns = [
    url(r'^api/v1/', include(apiurls)),
]