from datetime import datetime, timedelta
from random import choice

from django.test import TestCase
from venueapp.factories import BookingFactory

from venueapp.models import Area, Booking, Vehicle
from venueapp.utils import str_to_datetime


class UnitTest(TestCase):

    fixtures = ['fixtures.json']

    def test_consumption_at(self):
        """
            Tests an areas consumption or free space at a given time.
        """
        area1      = Area.objects.first()

        # Basic start times
        times = {
            'start_time': str_to_datetime('2016-03-23 00:00'),
            'end_time'  : str_to_datetime('2016-03-24 00:00')
        }

        timeslotted_objects = area1.timeslotted_objects(**times)

        [self.assertEqual(obj['free_spaceunits'], 7) and self.assertEqual(obj['free_parking'], 10)
            for obj in timeslotted_objects.viewvalues()]

        timeslotted_object = area1.timeslotted_objects(str_to_datetime('2016-03-24 13:00'))

        self.assertEqual(timeslotted_object['free_spaceunits'], 7)
        self.assertEqual(timeslotted_object['free_parking'], 10)

        timeslotted_object = area1.timeslotted_objects(str_to_datetime('2016-03-24 13:59'))

        self.assertEqual(timeslotted_object['free_spaceunits'], 7)
        self.assertEqual(timeslotted_object['free_parking'], 10)

        timeslotted_object = area1.timeslotted_objects(str_to_datetime('2016-03-24 14:00'))

        self.assertEqual(timeslotted_object['free_spaceunits'], 3)
        self.assertEqual(timeslotted_object['free_parking'], 9)

        timeslotted_object = area1.timeslotted_objects(str_to_datetime('2016-03-24 14:52'))

        self.assertEqual(timeslotted_object['free_spaceunits'], 3)
        self.assertEqual(timeslotted_object['free_parking'], 9)

        timeslotted_object = area1.timeslotted_objects(str_to_datetime('2016-03-24 14:53'))

        self.assertEqual(timeslotted_object['free_spaceunits'], 1)
        self.assertEqual(timeslotted_object['free_parking'], 8)

        # Insert new booking and check consumption
        BookingFactory(
            area=area1, start_time=str_to_datetime('2016-03-24 14:50'), end_time=str_to_datetime('2016-03-24 15:20'))

        timeslotted_object = area1.timeslotted_objects(str_to_datetime('2016-03-24 14:53'))

        self.assertEqual(timeslotted_object['free_spaceunits'], 0)
        self.assertEqual(timeslotted_object['free_parking'], 7)

        # check end of booking
        timeslotted_object = area1.timeslotted_objects(str_to_datetime('2016-03-24 15:20:00'))

        self.assertEqual(timeslotted_object['free_spaceunits'], 0)
        self.assertEqual(timeslotted_object['free_parking'], 7)

        timeslotted_object = area1.timeslotted_objects(str_to_datetime('2016-03-24 15:20:01'))

        self.assertEqual(timeslotted_object['free_spaceunits'], 1)
        self.assertEqual(timeslotted_object['free_parking'], 8)

    def test_can_accept_at(self):
        """
            Tests availability at given time
        """
        area1      = Area.objects.first()
        van        = Vehicle.objects.create(plate='asd', vehicle_type=Vehicle.VAN)

        # Request params for a VAN
        times = {
            'start_time': str_to_datetime('2016-03-24 15:50:00')
        }

        # get available times &
        # get random available timeslot
        params = {
            'start_time'   : times['start_time'],
            'end_time'     : times['start_time'] + timedelta(hours=2),
            'vehicle_type' : van.vehicle_type
        }
        selected_slot = choice(area1.free_timeslots(**params))

        # get available amenites
        timeslotted_object = \
            area1.timeslotted_objects(selected_slot[0], selected_slot[1])[(selected_slot[0], selected_slot[1])]

        for _ in range(timeslotted_object['free_spaceunits'] - 1):
            BookingFactory(area=area1, start_time=selected_slot[0], end_time=selected_slot[1])

        BookingFactory(area=area1, start_time=selected_slot[0], end_time=selected_slot[1])

        # try and book again
        with self.assertRaises(Exception):
            BookingFactory(area=area1, start_time=selected_slot[0], end_time=selected_slot[1])

        # assert the slot is not in available timeslots anymore
        self.assertNotIn(selected_slot, area1.free_timeslots(**params))
