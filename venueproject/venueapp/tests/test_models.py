from django.test import TestCase
from django.utils import timezone

from datetime import datetime
from django.utils.timezone import utc

from venueapp.models import *
from venueapp.factories import AreaFactory, BookingFactory
from venueapp.utils import str_to_datetime


class ModelsTest(TestCase):

    def setUp(self):

        ven0 = Venue.objects.create(name='Venue 1', min_duration=timedelta(hours=1))
        ven1 = Venue.objects.create(name='Venue 2', min_duration=timedelta(minutes=30))
        ven3 = Venue.objects.create(name='Venue 3', min_duration=timedelta(minutes=30))

        Area.objects.create(description='area0', max_spaceunits=200, venue=ven0)
        Area.objects.create(description='area0', venue=ven1)
        Area.objects.create(description='area1', venue=ven1, max_vehicles=50)
        Area.objects.create(description='small area', max_vehicles=1, venue=ven3)

        Vehicle.objects.create(plate='00', vehicle_type=Vehicle.CAR)
        Vehicle.objects.create(plate='01', vehicle_type=Vehicle.TRUCK)
        Vehicle.objects.create(plate='02', vehicle_type=Vehicle.VAN)

    def test_venue_and_areas(self):
        """
            Basic testing of relationship between venues and areas
        """
        ven0 = Venue.objects.get(name='Venue 1')
        ven1 = Venue.objects.get(name='Venue 2')

        self.assertEqual(ven0.areas.count(), Area.objects.filter(venue=ven0).count())
        self.assertEqual(ven1.areas.count(), Area.objects.filter(venue=ven1).count())

        area00 = Area.objects.filter(venue=ven0).first()

        self.assertIn(area00, ven0.areas.all())
        self.assertNotIn(area00, ven1.areas.all())

    def test_area(self):
        """
            Basic testing of Validation for area
        """
        ven0   = Venue.objects.get(name='Venue 1')
        area00 = Area.objects.filter(venue=ven0).first()
        timeslotted_objects = area00.timeslotted_objects(
            timezone.now(),
            timezone.now() + timedelta(hours=1)
        )

        [self.assertEqual(obj['free_spaceunits'], 200) and self.assertEqual(obj['free_parking'], 200)
         for obj in timeslotted_objects.viewvalues()]

    def test_booking(self):
        """
            Basic testing of Booking capabilities
        """

        ven1   = Venue.objects.get(name='Venue 2')
        area01 = Area.objects.filter(venue=ven1).first()
        car    = Vehicle.objects.get(plate='00')
        truck  = Vehicle.objects.get(plate='02')

        start = timezone.now()
        end   = start - timedelta(hours=1)

        # create a booking with unaccetable dates
        with self.assertRaises(Exception):
            Booking.objects.create(start_time=start, end_time=end, vehicle=car, area=area01)

        # create a booking with unaccetable duration and acceptable times
        end   = start + timedelta(minutes=1)
        with self.assertRaises(Exception):
            Booking.objects.create(start_time=start, end_time=end, vehicle=car, area=area01)

        booking_count = Booking.objects.filter(area=area01).count()
        area01.max_vehicles = booking_count + 1
        end   = start + timedelta(minutes=31)

        # Working booking
        Booking.objects.create(start_time=start, end_time=end, vehicle=car, area=area01)

        # create a booking wich overflows vehicles
        with self.assertRaises(Exception):
            Booking.objects.create(start_time=start, end_time=end, vehicle=car, area=area01)

        # booking count should still be the same + the first working booking(b0)
        self.assertEqual(booking_count + 1, Booking.objects.filter(area=area01).count())

        # create a booking wich overflows unit slots
        area01.max_spaceunits = 3
        with self.assertRaises(Exception):
            Booking.objects.create(start_time=start, end_time=end, vehicle=truck, area=area01)

        # booking count should still be the same
        self.assertEqual(booking_count + 1, Booking.objects.filter(area=area01).count())

    def test_capacity_across_times(self):
        """
            Tests that an Area's capacity on one day does not affect its capacity
            on another.
        """
        venue = Venue.objects.get(name='Venue 3')

        # This Area can only accept one booking at a time
        small_area = venue.areas.filter(description='small area').get()

        january_times = {
            'start_time': datetime(2020, 1, 1, 8, tzinfo=utc),
            'end_time': datetime(2020, 1, 1, 9, tzinfo=utc)
        }

        february_times = {
            'start_time': datetime(2020, 2, 1, 8, tzinfo=utc),
            'end_time': datetime(2020, 2, 1, 9, tzinfo=utc)
        }

        vehicle = Vehicle.objects.get(plate='00')

        Booking.objects.create(
            vehicle=vehicle,
            area=small_area,
            **january_times
        )

        february_booking = Booking.objects.create(
            vehicle=vehicle,
            area=small_area,
            **february_times
        )

        # There are two Bookings at the small Area.
        self.assertEqual(small_area.bookings.count(), 2)

        self.assertEqual(small_area.bookings.filter(start_time=february_times['start_time'])[0], february_booking)
    
    def test_independence_of_booked_intervals(self):
        """
        Intervals which don't overlap should not count towards the same total.
        """

        def make_booking(**kwargs):
            """
            Make a Booking using the given arguments, converting start_time and
            end_time from strings to datetimes if needed.
            """

            if 'start_time' in kwargs and isinstance(kwargs['start_time'], str):
                kwargs['start_time'] = str_to_datetime(kwargs['start_time'])

            if 'end_time' in kwargs and isinstance(kwargs['end_time'], str):
                kwargs['end_time'] = str_to_datetime(kwargs['end_time'])

            return BookingFactory(**kwargs)

        area = AreaFactory(max_vehicles=2)

        make_booking(area=area,
                     start_time='2020-01-01 08:00',
                     end_time='2020-01-01 08:30')

        make_booking(area=area,
                     start_time='2020-01-01 08:30',
                     end_time='2020-01-01 09:00')

        make_booking(area=area,
                     start_time='2020-01-01 09:30',
                     end_time='2020-01-01 10:00')

        make_booking(area=area,
                     start_time='2020-01-01 08:00',
                     end_time='2020-01-01 10:00')

        with self.assertRaises(Exception):
            make_booking(area=area, start_time='2020-01-01 08:00', end_time='2020-01-01 10:00')
