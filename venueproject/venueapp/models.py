from __future__ import unicode_literals
from django.core.exceptions import ValidationError

from django.db import models
from datetime import timedelta
from uuid import uuid4


class Venue(models.Model):
    name = models.CharField('Name of the venue', max_length=200, blank=None, unique=True)
    min_duration = models.DurationField('Minimum time interval for a booking (in seconds or in hh:mm:ss format)',
                                        default=timedelta(minutes=30))

    def __str__(self):
        return '%s, min duration: %s minutes' % (self.name, self.min_duration)


class Area(models.Model):
    description = models.CharField('CarPark (Area) name', max_length=200, blank=None, null=None)
    max_spaceunits = models.PositiveIntegerField('Max space units accepted', default=400)
    check_vehicles = models.BooleanField('Check or not number of vehicles in area', default=True)
    max_vehicles = models.PositiveIntegerField('Max number of vehicles accepted', default=200)
    venue = models.ForeignKey(Venue, related_name='areas')

    def return_bookings(self, start, end):
        if start == end or end is None:
            return Booking.objects.filter(area=self, start_time__lte=start, end_time__gte=start)
        return Booking.objects.filter(area=self, start_time__lt=end, end_time__gt=start)

    def timeslotted_objects(self, start_time, end_time=None):
        """
            Returns the status of an area in a specific point in time
        """
        if end_time is not None:
            if start_time > end_time:
                raise Exception('Start time cannot be after end time')

        bookings = self.return_bookings(start_time, end_time)

        timeslotted_obj = {}

        if start_time == end_time or end_time is None:
            spaceunits_used = sum([booking.vehicle.vehicle_type for booking in bookings])
            vehicles_parked = bookings.count()

            timeslotted_obj = {
                'free_spaceunits': self.max_spaceunits - spaceunits_used,
                'free_parking': self.max_vehicles - vehicles_parked
            }
        else:
            starting_from = start_time
            while starting_from < end_time:

                bookings_in_current_slot = [
                    booking for booking in bookings if (
                        booking.start_time < starting_from + self.venue.min_duration and
                        booking.end_time > starting_from)]

                starting_from += self.venue.min_duration

                spaceunits_used = sum([booking.vehicle.vehicle_type for booking in bookings_in_current_slot])
                vehicles_parked = len(bookings_in_current_slot)

                timeslotted_obj[(starting_from - self.venue.min_duration, starting_from)] = {
                    'free_spaceunits': self.max_spaceunits - spaceunits_used,
                    'free_parking': self.max_vehicles - vehicles_parked
                }

        return timeslotted_obj

    def free_timeslots(self, start_time, end_time, vehicle_type):
        """
        Fetch available timeslot.

        :arg: start_time
        :arg: end_time
        :arg: vehicle_type

        :returns: list of available timeslots
        """

        if end_time is not None:
            if start_time > end_time:
                raise Exception('Start time cannot be after end time')

        timeslotted_objects = self.timeslotted_objects(start_time, end_time)
        timeslots = []

        for time_slot_tuple, area_obj in timeslotted_objects.viewitems():
            if self.check_vehicles:
                if area_obj['free_parking'] == 0 or area_obj['free_parking'] - 1 < 0:
                    continue
            if area_obj['free_spaceunits'] == 0 or area_obj['free_spaceunits'] - vehicle_type < 0:
                continue

            timeslots.append(list(time_slot_tuple))
        return timeslots

    def __str__(self):
        return '%s, max spaceunits: %s -  %s' % (self.description, self.max_spaceunits, self.venue)


class Vehicle(models.Model):
    CAR = 1
    VAN = 2
    TRUCK = 4

    VEHICLE_CHOICES = (
        (CAR, 'CAR'),
        (VAN, 'VAN'),
        (TRUCK, 'TRUCK')
    )

    plate = models.CharField('Vehicle Plate', max_length=100, unique=True)
    vehicle_type = models.IntegerField('Vehicle Type', choices=VEHICLE_CHOICES, default=CAR)

    def __str__(self):
        return '%s(%s)' % (self.plate, dict(self.VEHICLE_CHOICES)[self.vehicle_type])


class Booking(models.Model):
    uuid = models.UUIDField('Booking Identifier', default=uuid4, editable=False)
    start_time = models.DateTimeField('Start datetime (isoformat)')
    end_time = models.DateTimeField('End datetime (isoformat)')
    area = models.ForeignKey(Area, related_name='bookings')
    vehicle = models.ForeignKey(Vehicle, related_name='bookings')

    # Basic check on the date passed to the model, even though must of the BL should be elsewhere
    def save(self, *args, **kwargs):
        if self.start_time > self.end_time:
            raise ValidationError('Start time cannot be after end time')
        if self.end_time - self.start_time < self.area.venue.min_duration:
            raise ValidationError('Min duration for a reservation is %s' % self.area.venue.min_duration,
                                  params={'min_duration': str(self.area.venue.min_duration)})

        # If no timeslots at all, no booking
        free_timeslots = self.area.free_timeslots(self.start_time, self.end_time, self.vehicle.vehicle_type)
        if not free_timeslots:
            raise ValidationError('No bookings available. Try to book in another area or time period')

        # We could also count the number of elements in free_timeslots is equal to the necessary number
        # of slots during start_time & end_time (even though that wouldn't be failureproof)

        # If no available timeslots or "hole" in timeslots that needs to be available in time range
        # No booking
        starting_from = self.start_time
        min_duration = self.area.venue.min_duration
        while starting_from < self.end_time:
            if [starting_from, starting_from + min_duration] not in free_timeslots:
                raise ValidationError('No bookings available. Try to book in another area or time period')
            starting_from += min_duration

        super(Booking, self).save(*args, **kwargs)

    def __str__(self):
        return '%s, from %s to %s' % (self.uuid, self.start_time, self.end_time)
