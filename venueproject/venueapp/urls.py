from venueapp.views import bookings, areas, venues, vehicles, intervals
from django.conf.urls import url

urlpatterns = [
    url(r'^bookings/$', bookings.BookingList.as_view(), name='bookings-list'),
    url(r'^bookings/(?P<uuid>([^/]+))$', bookings.BookingDetail.as_view(), name='booking-detail')
]

urlpatterns += [
    url(r'^areas/$', areas.AreaList.as_view(), name='areas-list'),
    url(r'^areas/(?P<pk>([^/]+))/$', areas.AreaDetail.as_view(), name='areas-detail'),
]

urlpatterns += [
    url(r'^venues/$', venues.VenueList.as_view(), name='venues-list'),
    url(r'^venues/(?P<pk>([^/]+))/$', venues.VenueDetail.as_view(), name='venues-detail'),
]


urlpatterns += [
    url(r'^vehicles/$', vehicles.VehicleList.as_view(), name='vehicles-list'),
    url(r'^vehicles/(?P<plate>([^/]+))/$', vehicles.VehicleDetail.as_view(), name='vehicles-detail'),
]

urlpatterns += [
    url(r'^availability/$', intervals.AvailableList.as_view(), name='available-list'),
]
