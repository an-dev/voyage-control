from django.core.exceptions import ValidationError
from django.db import transaction
from rest_framework import serializers, generics, status
from rest_framework.response import Response
from venueapp.models import Booking, Area, Vehicle


class BookingSerializer(serializers.ModelSerializer):

    area     = serializers.PrimaryKeyRelatedField(many=False, queryset=Area.objects.all())
    vehicle  = serializers.SlugRelatedField(many=False, slug_field='plate', required=True, queryset=Vehicle.objects.all())

    class Meta:
        model = Booking
        fields = ('uuid', 'start_time', 'end_time', 'area', 'vehicle')


class BookingList(generics.ListCreateAPIView):

    serializer_class = BookingSerializer
    queryset         = Booking.objects.all()
    ordering_fields  = ('area', 'start_time', 'end_time', 'vehicle')
    lookup_field     = 'uuid'

    def create(self, request, *args, **kwargs):
        try:
            with transaction.atomic():
                response = super(BookingList, self).create(request, *args, **kwargs)
        except ValidationError as ve:
            print ve
            response = Response({
                'detail': ve.message
            }, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print e
            response = Response({
                'detail': e.message
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return response


class BookingDetail(generics.RetrieveUpdateDestroyAPIView):

    serializer_class = BookingSerializer
    queryset         = Booking.objects.all()
    lookup_field     = 'uuid'

    def update(self, request, *args, **kwargs):
        try:
            with transaction.atomic():
                response = super(BookingDetail, self).update(request, *args, **kwargs)
        except ValidationError as ve:
            print ve
            response = Response({
                'detail': ve.message
            }, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            print e
            response = Response({
                'detail': e.message
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return response


