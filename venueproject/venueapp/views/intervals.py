from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from venueapp.models import Area, Vehicle
from venueapp.utils import str_to_datetime


class AvailableList(APIView):

    def get(self, request):
        try:
            accepted_keys = ['vehicle_type', 'area', 'start_time', 'end_time']
            missing_keys  = []
            while len(accepted_keys) > 0:
                cur_key = accepted_keys.pop()
                if cur_key not in request.query_params.viewkeys():
                    missing_keys.append(cur_key)

            if len(missing_keys) > 0:
                raise IndexError('Missing keys %s' % ', '.join([str(k) for k in missing_keys]))

            area_pk      = request.query_params['area']
            vehicle_type = request.query_params['vehicle_type']
            start_time   = request.query_params['start_time']
            end_time     = request.query_params['end_time']

            if type(vehicle_type) in [basestring, unicode]:
                if vehicle_type.upper() not in dict(Vehicle.VEHICLE_CHOICES).viewvalues():
                    raise IndexError('Unaccetable vehicle string %s' % vehicle_type)
                vehicle_type = [k for k, v in dict(Vehicle.VEHICLE_CHOICES).viewitems() if v == vehicle_type.upper()][0]
            else:
                if vehicle_type not in dict(Vehicle.VEHICLE_CHOICES).viewkeys():
                    raise IndexError('Unaccetable vehicle value %s' % vehicle_type)

            start_time   = str_to_datetime(start_time)
            end_time     = str_to_datetime(end_time)

            area = Area.objects.get(pk=area_pk)
            response = Response({
                'detail': area.free_timeslots(start_time, end_time, vehicle_type)
            }, status=status.HTTP_200_OK)
        except IndexError as ie:
            print ie
            response = Response({
                'detail': ie.message
            }, status=status.HTTP_400_BAD_REQUEST)
        except Area.DoesNotExist as ae:
            print ae
            response = Response({
                'detail': ae.message
            }, status=status.HTTP_404_NOT_FOUND)
        except Exception as e:
            print e
            response = Response({
                'detail': 'Unexpected server error: %s' % e.message
            }, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return response

