from rest_framework import serializers, generics
from venueapp.models import Vehicle


class VehicleSerializer(serializers.ModelSerializer):

    bookings     = serializers.SlugRelatedField(many=True, read_only=True, slug_field='uuid')
    vehicle_type_str = serializers.SerializerMethodField('return_vehicle_type')

    class Meta:
        model = Vehicle
        fields = ('plate', 'vehicle_type', 'vehicle_type_str', 'bookings')

    def return_vehicle_type(self, obj):
        return dict(Vehicle.VEHICLE_CHOICES)[obj.vehicle_type]


class VehicleList(generics.ListCreateAPIView):

    serializer_class = VehicleSerializer
    queryset         = Vehicle.objects.all()
    ordering_fields  = ('plate',)
    lookup_field     = 'plate'


class VehicleDetail(generics.RetrieveUpdateDestroyAPIView):

    serializer_class = VehicleSerializer
    queryset         = Vehicle.objects.all()
    lookup_field     = 'plate'
