

# Create your views here.
from rest_framework import serializers, generics
from venueapp.models import Venue
from venueapp.views.areas import AreaSerializer


class VenueSerializer(serializers.ModelSerializer):

    areas = AreaSerializer(many=True, read_only=True)

    class Meta:
        model = Venue
        fields = ('pk', 'name', 'min_duration', 'areas')


class VenueList(generics.ListCreateAPIView):

    serializer_class = VenueSerializer
    queryset         = Venue.objects.all()
    ordering_fields  = ('name',)


class VenueDetail(generics.RetrieveUpdateDestroyAPIView):

    serializer_class    = VenueSerializer
    queryset            = Venue.objects.all()
    lookup_field        = 'pk'
