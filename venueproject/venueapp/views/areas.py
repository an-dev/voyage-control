from rest_framework import serializers, generics
from venueapp.models import Area


class AreaSerializer(serializers.ModelSerializer):

    bookings     = serializers.SlugRelatedField(many=True, slug_field='uuid', read_only=True)

    class Meta:
        model = Area
        fields = ('pk', 'description', 'max_spaceunits',
                  'check_vehicles', 'max_vehicles', 'venue', 'bookings')


class AreaList(generics.ListCreateAPIView):

    serializer_class = AreaSerializer
    queryset         = Area.objects.all()
    ordering_fields  = ('venue', 'description')


class AreaDetail(generics.RetrieveUpdateDestroyAPIView):

    serializer_class = AreaSerializer
    queryset         = Area.objects.all()
    lookup_field     = 'pk'
