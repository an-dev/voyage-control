import factory
from venueapp.models import Venue, Area, Vehicle, Booking


class VenueFactory(factory.DjangoModelFactory):
    
    name = factory.Sequence(lambda n: 'Venue %d' % n)
    
    class Meta:
        model = Venue


class AreaFactory(factory.DjangoModelFactory):
    
    venue = factory.SubFactory(VenueFactory)
    
    description = factory.Sequence(lambda n: 'Area %d' % n)
    
    class Meta:
        model = Area


class VehicleFactory(factory.DjangoModelFactory):
    
    plate = factory.Sequence(lambda n: ('%d' % n).zfill(6))
    
    class Meta:
        model = Vehicle


class BookingFactory(factory.DjangoModelFactory):
    
    area = factory.SubFactory(AreaFactory)
    
    vehicle = factory.SubFactory(VehicleFactory)
    
    class Meta:
        model = Booking
