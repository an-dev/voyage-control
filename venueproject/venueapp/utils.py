from dateutil import parser
from django.utils.dateparse import parse_datetime
from django.utils.timezone import utc


def str_to_datetime(dt_string):
    """
    Convert a string to a UTC datetime.
    """
    # return parse_datetime(dt_string).replace(tzinfo=utc)
    return parser.parse(dt_string).replace(tzinfo=utc)
