## Data model for the states of a Venue's resources

*Follow up on our discussion on Friday*

> Any car park has a set of capacities.  For example, once capacity for a given car park might be 200 units of space, where different vehicle types take up different units (e.g. a truck takes 4, a van takes 2, a car takes 1), and another for the same car park might be a maximum of 100 vehicles (irrespective of type).  There will be several car parks within a Venue, and different car parks will be considered better for accessing different areas within the building.

Since it wasn't specified that different car parks belonging to the same venue *could* have different constraints, I'm assuming for this case thet they do have all the same capacities and/or maximum car units.

The possible rappresentation of the resources can be seen in the image attached.


> The process for (A) is: the user (client app) specifies a time range (perhaps up to several days), and a number of other values, say: a vehicle type, a desired duration, and an area within the building which they want to access.  The server must then return a list of time intervals which are available to be booked.  (Time intervals will typically be to the nearest 30 minutes, so e.g. if an hour duration is requested, the returned time intervals will be of the form [[10:00, 11:00], [10:30, 11:30]], but this 30 minutes needs to be configurable per venue, down to the minute or even second level.)

For case A the backend app could simply query the database on the various fields taking care of these things:

* Vehicle type: a truck (for example) takes up 4 units of space, but these unit spaces **must** be subsequent (we can't place a truck in a 2+2 unit space)
* Configurable Venue reservation Time: It wasn't specified if these times should be for the same parking space or could be from different areas of the same venue (eg.: [10:30 - 11:00 park in area1, 11:00 - 11:30 park in area2]), on which I assume they must be for the same area, as in the contrary this could result a little troublesome for the drivers loading/unloading materials and such.

> For (B), the user sends the same information as in (A), but also specifies one of the time intervals, e.g. [10:00, 11:00].  The server must (assuming that interval is still available) then allocate an appropriate amount of space in a car park during the interval.

For case B the process should be the same, leaving even the place for a little suggestion for the client:

*e.g.:* time interval = [10:00, 11:00] with vehicle type = van and area = 1

The backend app could return a suggestion if there are no available spots for that time interval or in that specific area.
